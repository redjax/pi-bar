"""Testing area for pickle function,
hopefully to pass values to Flask."""
import pickle

import jsonop
import weather


def openpickle(file):
    """Open a pickle object."""
    data = jsonop.unpickle(file)
    return data


def printvalue(data, layer, val):
    """Try to print specific value from pickle."""
    print(data[layer][val])


def return_data(file, layer, val):
    dataset = openpickle(file)
    return dataset[layer][val]


def return_data2(file, layer, layer2, val):
    dataset = openpickle(file)
    return dataset[layer][layer2][val]


def return_single_data(file, val):
    dataset = openpickle(file)
    return dataset[val]


def return_temp(file):
    tempval = return_data(file, 'main', 'temp')
    return tempval


def return_descrip(file):
    descripval = return_data2(file, 'weather', 0, 'description')
    return descripval


def return_mintemp(file):
    mintempval = return_data(file, 'main', 'temp_min')
    return mintempval


def return_maxtemp(file):
    maxtempval = return_data(file, 'main', 'temp_max')
    return maxtempval


def return_city(file):
    cityval = return_single_data(file, 'name')
    return cityval


"""Example Dict
{
    'coord':
        {
            'lon': -81.69,
            'lat': 41.51
        },
    'weather':
        [
            {
                'id': 600,
                'main': 'Snow',
                'description': 'light snow',
                'icon': '13n'
            }
        ],
    'base': 'stations',
    'main':
        {
            'temp': 19.4,
            'pressure': 1031,
            'humidity': 85,
            'temp_min': 17.6,
            'temp_max': 21.2
        },
    'visibility': 16093,
    'wind':
        {
            'speed': 6.06,
            'deg': 332.501},
    'clouds':
        {
            'all': 90
        },
    'dt': 1550537700,
    'sys':
        {
            'type': 1, 'id': 3455,
            'message': 0.004,
            'country': 'US',
            'sunrise': 1550578594,
            'sunset': 1550617511
        },
    'id': 5150529,
    'name': 'Cleveland',
    'cod': 200
}
"""
