"""Cleanup and maintenance for project.

Functions to clean up old data, manage temporary files, etc.
"""
import configparser
import datetime
import os
import shutil


def cleanup(file):
    """Clean passed folder of data."""
    # File to be deleted.
    if os.path.exists(file):
        os.remove(file)


def trimhistory(folder):
    """Archive files older than timelimit."""
    # Folder to scan
    datafolder = folder
    # Get list of files in datafolder
    filelist = os.listdir(datafolder)
    # Count how many files in datafolder
    filecount = len(filelist)
    # How long to keep files before moving to archive
    timelimit = datetime.timedelta(hours=24)

    if filecount > 5:
        for dirpath, dirname, filenames in os.walk(datafolder):
            for file in filenames:
                curpath = os.path.join(datafolder, file)
                file_modified = datetime.datetime.fromtimestamp(
                    os.path.getmtime(curpath))
                if datetime.datetime.now() - file_modified > timelimit:
                    shutil.move(curpath, folder + '/archive')


def trimhistory2(folder):
    """Rewrite trimhistory function for pickled files."""
    datafolder = folder
    archivefolder = '../data/archive'
    filelist = os.listdir(datafolder)
    filecount = len(filelist)
    timelimit = datetime.timedelta(hours=24)

    if filecount > 5:
        for dirpath, dirname, filenames in os.walk(datafolder):
            for file in filenames:
                curpath = os.path.join(datafolder, file)
                file_modified = datetime.datetime.fromtimestamp(
                    os.path.getmtime(curpath)
                )
                if datetime.datetime.now() - file_modified > timelimit:
                    shutil.move(curpath, archivefolder)


def projectsetup():
    """Create folders and files on new PC/at runtime."""
    # Folders that don't get pushed because of gitignore
    folders = ['../data', '../data/history',
               '../data/history/archive',
               '../data/temp', '../data/pickled']
    # Files ignored by gitignore
    files = ['../data/config.ini']
    # Data to be written to configdata
    configdata = ["[openweathermap]",
                  "api=enter your api key here",
                  "temp_unit=enter temperature unit here",
                  "[settings]",
                  "beenrun=1"
                  ]

    # Check if folder exists, create if not
    for folder in folders:
        if not os.path.exists(folder):
            os.makedirs(folder)

    # Check if file exists, create if not
    for file in files:
        if not os.path.exists(file):
            open(file, "a")

    # Create config.ini if it does not exist
    outfile = open('../data/config.ini', "a")

    # Loop through configdata list, write each line to config.ini
    for line in configdata:
        outfile.write(line)
        outfile.write("\n")  # New line for neat formatting
    outfile.close()


def check_config_val(conffile, section, val):
    """Checks specified config file for given value/setting."""
    config = configparser.ConfigParser()
    try:
        config.read(conffile)
        return config[section][val]
    except FileNotFoundError as fnf_error:
        return fnf_error
        projectsetup()


def createfile(file):
        """Creates file if it does not already exist."""


projectsetup()
