"""
Since there's no official way to update all packages with Pip, this script
will create a list of all pip packages, and run the update command on each
package.

Solution found here: https://stackoverflow.com/a/5839291
"""
from subprocess import call

import pkg_resources

packages = [dist.project_name for dist in pkg_resources.working_set]
call("pip install --upgrade " + ' '.join(packages), shell=True)
