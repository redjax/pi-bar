@echo off
REM Change directory to git folder containing project
SET PROJECT="pi-bar"

cd ..\..\..\
REM Create the virtualenv
virtualenv %PROJECT%
REM Change directory to project root
cd %PROJECT%\
REM Start the virtualenv
CALL Scripts\activate
REM Change to the maint directory
cd app\maint\
REM Install requirements with pip
python installreqs.py
