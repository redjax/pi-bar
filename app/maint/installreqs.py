import os
# import shutil
from subprocess import call

rootdir = '../../'  # The root 'swappascrape' dir
appdir = 'app'  # The 'app' directory
maintdir = appdir + '/maint'
# Get the current directory with: os.path.abspath(os.curdir)

os.chdir(rootdir)  # cd to root dir before running pip
call('pip install -r requirements.txt')  # Open shell, install reqs with pip

# os.chdir(maintdir)  # cd to maint dir before running postgitpull script
# call('python postgitpull.py')  # Run the postgitpull script
# call('deactivate')  # Close the venv
