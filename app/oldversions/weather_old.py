import configparser
import datetime
import re

import requests

import jsonop
from maint import maint


def get_api_key():
    config = configparser.ConfigParser()
    config.read('data/config.ini')

    try:
        return config['openweathermap']['api']
    except FileNotFoundError as fnf_error:
        print(fnf_error)
        maint.projectsetup()


def get_weather(api_key, location):
    """Get weather for location passed as arg."""
    # Split URL over 2 lines for building URL.
    formattedtime = formatdatetime()
    filename = formattedtime + location

    def buildurl():
        """Build URL to pass as request."""
        # Load temp unit (imperial/metric) from config
        temp_unit = get_temp_unit()
        # Base URL to build on
        url = "https://api.openweathermap.org/data/2.5/weather?"
        # Format conditionals
        urladd = "q={}&units={}&appid={}".format(location, temp_unit, api_key)
        completeurl = url + urladd  # Build URL

        return completeurl

    def get_temp_unit():
        """Get preferred unit(s)."""
        # Set configparser as variable
        config = configparser.ConfigParser()
        # Open config file for reading
        config.read('data/config.ini')

        try:
            # Return temperature unit from config
            return config['openweathermap']['temp_unit']
        except FileNotFoundError as fnf_error:
            # File not found/created
            print(fnf_error)
            # Run maintenance script to create project environment
            maint.projectsetup()

    # Make API request for weather data
    request = requests.get(buildurl())
    # Format returned data to JSON for writing
    request = request.json()
    # Write to file
    jsonop.pickleobject(request, filename)
    return request


def formatdatetime():
    """Format date and time for writing to JSON."""
    # Get time/date at time of function call
    now = datetime.datetime.now()
    # Format output to YYYY-mm-dd_HH:MM:SS
    formatted = now.strftime("%Y-%m-%d_%H-%M-%S_")

    return formatted


def maint_functions():
    """Maintenance functions to run after each program run."""
    # Move old files to archive
    # Don't enable until logic in place for handling "archive" folder
    # maint.trimhistory('data/history')
    # Clean up/delete temporary data
    # List with files to clean
    tempdata = ['data/temp/query.json']
    for item in tempdata:
        maint.cleanup(item)


def return_data(weatherdata, datarequest):
    """Return weatherdata, pass to Flask template."""
    longitude = weatherdata['coord']['lon']
    latitude = weatherdata['coord']['lat'],
    weathertype = weatherdata['weather'][0]['main'],
    description = weatherdata['weather'][0]['description'],
    temperature = weatherdata['main']['temp'],
    airpressure = weatherdata['main']['pressure'],
    humidity = weatherdata['main']['humidity'],
    mintemp = weatherdata['main']['temp_min'],
    maxtemp = weatherdata['main']['temp_max'],
    sunrise = weatherdata['sys']['sunrise'],
    sunset = weatherdata['sys']['sunset'],
    city = weatherdata['name']

    if datarequest == 'longitude':
        return strip_chars(longitude)
    elif datarequest == 'latitude':
        return strip_chars(latitude)
    elif datarequest == 'weathertype':
        return strip_chars(weathertype)
    elif datarequest == 'description':
        return strip_chars(description)
    elif datarequest == 'temperature':
        return strip_chars(temperature)
    elif datarequest == 'airpressure':
        return strip_chars(airpressure)
    elif datarequest == 'humidity':
        return strip_chars(humidity)
    elif datarequest == 'mintemp':
        return strip_chars(mintemp)
    elif datarequest == 'maxtemp':
        return strip_chars(maxtemp)
    elif datarequest == 'sunrise':
        return strip_chars(sunrise)
    elif datarequest == 'sunset':
        return strip_chars(sunset)
    elif datarequest == 'city':
        return strip_chars(city)


def strip_chars(val):
        """Strip characters from return value."""
        # Set value to string for replacement
        val = str(val)
        chars = "\(|\)|\,"
        return re.sub(chars, '', val)


def prep_flask_pass(data):
        """Prep an item to pass to Flask."""
        """vals = ['longitude',
                'latitude',
                'weathertype',
                'description',
                'temperature',
                'airpressure',
                'humidity',
                'mintemp',
                'maxtemp',
                'sunrise',
                'sunset',
                'city']"""
        # Temporary list to use while testing
        tempvals = ['temperature', 'description']
        passvals = []

        for item in tempvals:
            count = 0
            while count < 2:
                additem = return_data(data, tempvals[count])
                passvals.append(additem)
                count += 1
        return passvals


def pass_to_flask(weatherdata):
    """Return list of weather items to Flask."""
    longitude = weatherdata['coord']['lon']
    latitude = weatherdata['coord']['lat'],
    weathertype = weatherdata['weather'][0]['main'],
    description = weatherdata['weather'][0]['description'],
    temperature = weatherdata['main']['temp'],
    airpressure = weatherdata['main']['pressure'],
    humidity = weatherdata['main']['humidity'],
    mintemp = weatherdata['main']['temp_min'],
    maxtemp = weatherdata['main']['temp_max'],
    sunrise = weatherdata['sys']['sunrise'],
    sunset = weatherdata['sys']['sunset'],
    city = weatherdata['name']

    pass_list = [longitude,
                 latitude,
                 weathertype,
                 description,
                 temperature,
                 airpressure,
                 humidity,
                 mintemp,
                 maxtemp,
                 sunrise,
                 sunset,
                 city]

    return pass_list


def main():
    """Run functions in order to get weather data."""
    # if len(sys.argv) != 2:  # Check that a city was specified
    # exit("Usage: {} LOCATION".format(sys.argv[0]))  # Exit w/ error
    # location = sys.argv[1]  # Set arg passed at runtime to location
    location = 'cleveland'

    api_key = get_api_key()  # Load API from config.ini

    # Get weather data
    # weatherdata = get_weather(api_key, location)
    get_weather(api_key, location)
    # Return data to Flask
    # temp = return_data(weatherdata, 'temperature')
    # return temp
    # pass_to_flask(weatherdata)

    # Run maintenance scripts
    # maint_functions()


# main()
