"""Pi-Bar "Smart" Bar

Code for my "pi-bar" project with Tom.
"""

import weatherpy
from maint import maint

beenrun = maint.check_config_val('data/config.ini', 'settings', 'beenrun')

# Check if setup has already been run
if beenrun == 1:
    pass
elif beenrun == 0:
    # maint.projectsetup()
    print("It does not appear setup has been run.")

weatherpy.main()
# maint.trtrimhistory('data/history/')
