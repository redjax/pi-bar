import datetime
import re
import sys

import requests

import jsonop
from maint import maint


def get_weather(api_key, location):
    """Get weather for location passed as arg."""
    # Split URL over 2 lines for building URL.
    formattedtime = formatdatetime()
    filename = formattedtime + location

    def buildurl():
        """Build URL to pass as request."""
        # Load temp unit (imperial/metric) from config
        temp_unit = get_temp_unit()
        # Base URL to build on
        url = "https://api.openweathermap.org/data/2.5/weather?"
        # Format conditionals
        urladd = "q={}&units={}&appid={}".format(location, temp_unit, api_key)
        completeurl = url + urladd  # Build URL

        return completeurl

    def get_temp_unit():
        """Get preferred unit(s)."""
        unit = jsonop.readconfig('data/config.ini', 'openweathermap',
                                 'temp_unit')
        return unit

    # Make API request for weather data
    request = requests.get(buildurl())
    # Format returned data to JSON for writing
    request = request.json()
    # Write to file
    jsonop.pickleobject(request, filename)
    return request


def formatdatetime():
    """Format date and time for writing to JSON."""
    # Get time/date at time of function call
    now = datetime.datetime.now()
    # Format output to YYYY-mm-dd_HH:MM:SS
    formatted = now.strftime("%Y-%m-%d_%H-%M-%S_")

    return formatted


def strip_chars(val):
        """Strip characters from return value."""
        # Set value to string for replacement
        val = str(val)
        chars = "\(|\)|\,"
        return re.sub(chars, '', val)


def maint_functions():
    """Maintenance functions to run after each program run."""
    # Move old files to archive
    # Don't enable until logic in place for handling "archive" folder
    # maint.trimhistory('data/history')
    # Clean up/delete temporary data
    # List with files to clean
    tempdata = ['data/temp/query.json']
    for item in tempdata:
        maint.cleanup(item)


if len(sys.argv) != 2:  # Check that a city was specified
    exit("Usage: {} LOCATION".format(sys.argv[0]))  # Exit w/ error

location = sys.argv[1]  # Set arg passed at runtime to location
