"""My own little wrapper for JSON operations.

Takes input as a JSON file argument, and performs
operations like reading, writing, and formatting.
"""

import configparser
import json
import pickle

from maint import maint


def writejson(data, fpath, fname):
    """Write JSON data to a file."""
    datafile = fpath + fname + '.json'
    with open(datafile, 'w+') as outfile:
        json.dump(data, outfile)


def readjson(file):
    """Read JSON from a file."""
    with open(file) as file:
        data = json.load(file)  # Load contents to var

    return data  # Return var


def pickleobject(data, file):
        """Serialize a data object to specified file."""
        # Folder to write pickled data to
        datafolder = 'data/pickled/'
        # Filename, including folder path
        file = datafolder + file
        # Open file with write attrib, write as binary
        f = open(file, 'wb+')
        # Pickle object
        pickle.dump(data, f)
        # Close file to avoid errors
        f.close()


def unpickle(file):
        """Unpickle file into data object."""
        # Open file with read attribute as binary
        f = open(file, 'rb')
        # Load data into an object
        data = pickle.load(f)
        # Close file to avoid errors
        f.close()

        # Return object with pickled data
        return data


def get_api_key():
    """Read config.ini, return API key."""
    apikey = readconfig('data/config.ini', 'openweathermap', 'api')
    return apikey


def readconfig(conf, section, data):
    """Read configuration from file."""
    config = configparser.ConfigParser()
    config.read(conf)

    try:
        return config[section][data]
    except FileNotFoundError as fnf_error:
        print(fnf_error)
        maint.projectsetup()
