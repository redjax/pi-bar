"""Flask App.

Flask app for weather functions.
"""
from flask import Flask, render_template

import pickletest
# import flasktests
import weather

app = Flask(__name__)
app.debug = True


@app.route('/')
def hello_world():
    return 'Hello World'


@app.route('/weather')
def send_weather():
    file = 'data/pickled/2019-02-18_20-39-40_cleveland'
    wtemp = pickletest.return_temp(file)
    wdescrip = pickletest.return_descrip(file)
    mintemp = pickletest.return_mintemp(file)
    maxtemp = pickletest.return_maxtemp(file)
    wcity = pickletest.return_city(file)

    return render_template('prototyping/weather.html',
                           wtemp=wtemp,
                           wdescrip=wdescrip,
                           mintemp=mintemp,
                           maxtemp=maxtemp,
                           wcity=wcity)


if __name__ == '__main__':
    app.run()
